package com.km.cms.model;

public enum RoleName {

	ROLE_ADMIN,
	ROLE_USER,
	ROLE_OPERATOR
}
